ardour {
	["type"]    = "session",
	name        = "Loop on Range Marker",
	license     = "MIT",
	author      = "Ardour Lua Task Force, Wargreen",
	description = [[A session script which auto loop on interval marker when rolling forward.]]
}

function factory ()
	return function (n_samples)
		if (not Session:transport_rolling ()) then
			-- not rolling, nothing to do.
			return
		end

		local pos = Session:transport_frame () -- current playhead position
		local loc = Session:locations () -- all marker locations
		local blk = Session:get_block_size ()

		-- find first marker after the current playhead position, ignore loop + punch ranges
		-- (this only works when rolling forward, to extend this see
		-- http://manual.ardour.org/lua-scripting/class_reference/#ARDOUR:Locations )
		local m = loc:first_mark_after (pos, false)

		if (m == -1) then
			-- no marker was found
			return
		else
		
			for l in loc:list ():iter () do
				if l:is_range_marker() and l:start() == m and l:start () > pos then

					-- since ardour may split the process cycle for events,
					-- n_samples may be smaller.

					-- transport modification can only happen on a process-cycle boundary.
					-- This callback happens from within the process callback,
					-- so we need to queue it ahead of time.
					if (pos + n_samples + blk >= m and pos + n_samples < m) then
						print("Range", l:name(), l:start(), l:length(), l:_end (), pos, pos + l:length ())
						loc:auto_loop_location ():set_start (pos + n_samples, false, false, 1)
						loc:auto_loop_location ():set_end (pos+n_samples + l:length (), false, false, 1)
						Session:request_play_loop (true, false)
					end
				end
			end

		end
	end
end
