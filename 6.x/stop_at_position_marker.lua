ardour {
	["type"]    = "session",
	name        = "Stop at Position Marker",
	license     = "MIT",
	author      = "Ardour Lua Task Force, Wargreen",
	description = [[A session script which stops the transport on position marker when rolling forward.]]
}

function factory ()
	return function (n_samples)
		if (not Session:transport_rolling ()) then
			-- not rolling, nothing to do.
			return
		end

		local pos = Session:transport_sample () -- current playhead position
		local loc = Session:locations () -- all marker locations

		-- find first marker after the current playhead position, ignore loop + punch ranges
		-- (this only works when rolling forward, to extend this example see
		-- http://manual.ardour.org/lua-scripting/class_reference/#ARDOUR:Locations )
		local m = loc:first_mark_after (pos, false)

		if (m == -1) then
			-- no marker was found
			return
		else
			-- check if the marker is a simple position marker, else return
			mt = loc:first_mark_at(m, 0)
			if (not mt) then
				return
			end
		end

		-- due to "first_mark_after" m is always > pos:
		-- assert(pos < m)
		-- so in the cycle that crosses "m" we need to stop at 'm'
		if (pos + n_samples >= m) then
			Session:request_locate (m, 1, ARDOUR.TransportRequestSource.TRS_Engine)
			--Session:request_locate (m, ARDOUR.LocateTransportDisposition.MustStop, ARDOUR.TransportRequestSource.TRS_Engine MustStop = 1;
		end
	end
end
