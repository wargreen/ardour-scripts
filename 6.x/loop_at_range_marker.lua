ardour {
	["type"]    = "session",
	name        = "Loop on Range Marker V2",
	license     = "MIT",
	author      = "Ardour Lua Task Force, x42, Wargreen",
	description = [[A session script which auto loop on interval marker when rolling forward. Create loop markers first !]]
}

function factory ()
	return function (n_samples)
		if (not Session:transport_rolling ()) then
			-- not rolling, nothing to do.
			return
		end

		local pos = Session:transport_sample () -- current playhead position
		local loc = Session:locations () -- all marker locations
		local blk = Session:get_block_size ()

		local m = loc:first_mark_after (pos, false)

		if Session:get_play_loop () then
			-- currently looping, nothing else to do
			return
		end

		if (m == -1) then
			-- no further markers, nothing to do
			return
		end

		if (pos + n_samples + blk >= m and pos + n_samples < m) then
			local loop_start = -1
			local loop_end = -1
			local llist = loc:list ()
			for l in llist:iter() do
				if l:start() == m then
					if l:is_range_marker() and l:start () > pos then
						loop_start = l:start()
						loop_end = l:_end()
						break
					end
				end
			end

			if loop_start < 0 or loop_end < 0 then
				return
			end

			print("Set loop", loop_start, loop_end)
			loc:auto_loop_location ():set_start (loop_start, true, false, 1)
			loc:auto_loop_location ():set_end (loop_end, true, false, 1)

			print("Loop start location ", loc:auto_loop_location():start())
			print("Loop end location ", loc:auto_loop_location():_end())

			print("Request looping")
			Session:request_play_loop (true, false)
		end
	end
end
