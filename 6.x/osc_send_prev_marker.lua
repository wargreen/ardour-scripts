ardour {
	["type"]    = "EditorHook",
	name        = "OSC Previous Marker Msg",
	author      = "Ardour Lua Task Force",
	description = "Send a OSC message with the previous marker name",
}

function action_params ()
	return
	{
		["uri"] = { title = "OSC URI ", default = "osc.udp://localhost:8080"},
		["path"] = { title = "OSC Path", default = "/prev_mark"},
	}
end

function signals ()
	s = LuaSignal.Set()
	s:add ({ [LuaSignal.LuaTimerDS] = true })
	return s
end

function factory (params)
	-- this function is evaluated every 100ms
	return function (signal, ref, ...)
--		if not Session:transport_rolling() then
--			return
--		end
		
		-- helper function to send OSC.
		function tx_osc (...) 
			local uri = params["uri"]
			local msg = {...}
			print("Send", msg[3], " to ", uri)

			local tx = ARDOUR.LuaOSC.Address (uri)
			tx:send (...)
		end
		
		
	
		local blk = Session:get_block_size ()
		local pos = Session:transport_sample () -- current playhead position
		local loc = Session:locations () -- all marker locations

		local m = -1
		local oscmsg = "Init..."
		
		if loc:mark_at (pos, 0) then
			print("We are on a marker")
			m = pos
		else
			print("Take previous marker")
			m = loc:first_mark_before (pos, false)
		end
		
		if (m == -1) then
			-- no marker was found
			print("No marker found")
			oscmsg = "No previous marker"

		else
			while not loc:mark_at (m, 0) do
				print("rewind")
				local prevm = loc:first_mark_before (m, false)
				if m == prevm then
					break
				else
					m = prevm
				end
			end
			
			if loc:mark_at (m, 0) then
				mark = loc:mark_at (m, 0)
				print("Previous mark @ ", m, " | Name : ", mark:name ())
				oscmsg = mark:name ()
				if m < pos then
					oscmsg = "<-- " .. oscmsg
				end
				
				if m == pos then
					oscmsg = "[ " .. oscmsg .. " ]"
				end
				
--			elseif mark:is_range_marker () then
--				TODO : Handle range marker
--			else
--				TODO : session range and more
			else 
				print("No mark found")
				oscmsg = "Only range before"
			end
			
		end
		tx_osc (params["path"], "s", oscmsg)
		
	end
end
