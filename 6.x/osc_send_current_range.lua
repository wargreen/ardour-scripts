ardour {
	["type"]    = "EditorHook",
	name        = "OSC Current range Msg",
	author      = "Ardour Lua Task Force",
	description = "Send a OSC message with current range name and time",
}

function action_params ()
	return
	{
		["uri"] = { title = "OSC URI ", default = "osc.udp://localhost:8080"},
		["path"] = { title = "OSC base Path", default = "/range"},
	}
end

function signals ()
	s = LuaSignal.Set()
	s:add ({ [LuaSignal.LuaTimerDS] = true })
	return s
end

function factory (params)
	-- this function is evaluated every 100ms
	return function (signal, ref, ...)
	
		-- helper function to send OSC.
		function tx_osc (...) 
			local uri = params["uri"]
			local msg = {...}
			print("Send", msg[3], " to ", uri, msg[1])

			local tx = ARDOUR.LuaOSC.Address (uri)
			tx:send (...)
		end
	
	
		local blk = Session:get_block_size ()
		local pos = Session:transport_sample () -- current playhead position
		local loc = Session:locations () -- all marker locations

		local osc_name_msg = "--"
		local osc_time_msg = 0
	
		function get_current_range ()
			local llist = loc:list ()
			for l in llist:iter() do
				if l:is_range_marker() and l:start () <= pos and l:_end() >= pos then
					return l
				end
			end
			return nil
		end
	
		local range = get_current_range ()
		if range then
			osc_name_msg = range:name()
			length = range:_end () - range:start ()
			osc_time_msg = 1 / length * (pos - range:start ())
		end
		
		tx_osc (params["path"] .. "/name", "s", osc_name_msg)
		tx_osc (params["path"] .. "/time", "f", osc_time_msg)
		
	end
end
